﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using cipher_api.Services;

namespace cipher_api.Controllers 
{
    [ApiController]
    [Route("[controller]")]
    public class CipherController : Controller
    {

        private ICipherRepository _cipherRepository;

        public CipherController(ICipherRepository cipherRepository)
        {
            _cipherRepository = cipherRepository;
        }


        [HttpGet("{shift}/{body}")]
        public IActionResult Get(int shift, String body)

        {


            List<Code> ciphers = new List<Code>();

            ciphers.Add(_cipherRepository.Caesar(shift, body));
            ciphers.Add(_cipherRepository.PlayFair(body));
            ciphers.Add(_cipherRepository.MD5(body));

            return Ok(ciphers);
            
        }
    }
}
