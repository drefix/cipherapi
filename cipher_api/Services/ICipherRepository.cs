﻿using System;

namespace cipher_api.Services
{
    public interface ICipherRepository
    {
        Code Caesar(int shift, String body);
        Code MD5(String body);
        Code PlayFair(String body);
    }
}
