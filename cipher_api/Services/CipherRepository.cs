﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace cipher_api.Services
{
    public class CipherRepository : ICipherRepository
    {
        static string Encrypt(string value, int shift)
        {
            if (string.IsNullOrEmpty(value))
                return value; // Nothing to encrypt

            // ensure, that shift is in [0..25] range
            shift = ((shift % 26) + 26) % 26;

            StringBuilder sb = new StringBuilder(value.Length);

            foreach (char letter in value)
                if (letter >= 'a' && letter <= 'z')
                    sb.Append((char)((letter - 'a' + shift) % 26 + 'a'));
                else if (letter >= 'A' && letter <= 'Z')
                    sb.Append((char)((letter - 'A' + shift) % 26 + 'A'));
                else
                    sb.Append(letter);

            return sb.ToString();
        }

        /* shift % 26 - in order to avoid integer overflow when shift == int.MinValue
         *
         * drefix - decrypt method
         * static string Decrypt(string value, int shift) =>
         * Encrypt(value, 26 - shift % 26);
         * 
         */

        Code ICipherRepository.Caesar(int shift, String body)
        {

            return new Code
            {
                body = Encrypt(body,shift),
                style = "Caesar"

            };
        }

        Code ICipherRepository.MD5(String body)
        {
			// step 1, calculate MD5 hash from input
			MD5 md5 = MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(body);
			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("X2"));
			}
			return new Code
			{
				body = sb.ToString(),
				style = "MD5"

			};
		}

		private static int Mod(int a, int b)
		{
			return (a % b + b) % b;
		}

		private static List<int> FindAllOccurrences(string str, char value)
		{
			List<int> indexes = new List<int>();

			int index = 0;
			while ((index = str.IndexOf(value, index)) != -1)
				indexes.Add(index++);

			return indexes;
		}

		private static string RemoveAllDuplicates(string str, List<int> indexes)
		{
			string retVal = str;

			for (int i = indexes.Count - 1; i >= 1; i--)
				retVal = retVal.Remove(indexes[i], 1);

			return retVal;
		}

		private static char[,] GenerateKeySquare(string key)
		{
			char[,] keySquare = new char[5, 5];
			string defaultKeySquare = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
			string tempKey = string.IsNullOrEmpty(key) ? "CIPHER" : key.ToUpper();

			tempKey = tempKey.Replace("J", "");
			tempKey += defaultKeySquare;

			for (int i = 0; i < 25; ++i)
			{
				List<int> indexes = FindAllOccurrences(tempKey, defaultKeySquare[i]);
				tempKey = RemoveAllDuplicates(tempKey, indexes);
			}

			tempKey = tempKey.Substring(0, 25);

			for (int i = 0; i < 25; ++i)
				keySquare[(i / 5), (i % 5)] = tempKey[i];

			return keySquare;
		}

		private static void GetPosition(ref char[,] keySquare, char ch, ref int row, ref int col)
		{
			if (ch == 'J')
				GetPosition(ref keySquare, 'I', ref row, ref col);

			for (int i = 0; i < 5; ++i)
				for (int j = 0; j < 5; ++j)
					if (keySquare[i, j] == ch)
					{
						row = i;
						col = j;
					}
		}

		private static char[] SameRow(ref char[,] keySquare, int row, int col1, int col2, int encipher)
		{
			return new char[] { keySquare[row, Mod((col1 + encipher), 5)], keySquare[row, Mod((col2 + encipher), 5)] };
		}

		private static char[] SameColumn(ref char[,] keySquare, int col, int row1, int row2, int encipher)
		{
			return new char[] { keySquare[Mod((row1 + encipher), 5), col], keySquare[Mod((row2 + encipher), 5), col] };
		}

		private static char[] SameRowColumn(ref char[,] keySquare, int row, int col, int encipher)
		{
			return new char[] { keySquare[Mod((row + encipher), 5), Mod((col + encipher), 5)], keySquare[Mod((row + encipher), 5), Mod((col + encipher), 5)] };
		}

		private static char[] DifferentRowColumn(ref char[,] keySquare, int row1, int col1, int row2, int col2)
		{
			return new char[] { keySquare[row1, col2], keySquare[row2, col1] };
		}

		private static string RemoveOtherChars(string input)
		{
			string output = input;

			for (int i = 0; i < output.Length; ++i)
				if (!char.IsLetter(output[i]))
					output = output.Remove(i, 1);

			return output;
		}

		private static string AdjustOutput(string input, string output)
		{
			StringBuilder retVal = new StringBuilder(output);

			for (int i = 0; i < input.Length; ++i)
			{
				if (!char.IsLetter(input[i]))
					retVal = retVal.Insert(i, input[i].ToString());

				if (char.IsLower(input[i]))
					retVal[i] = char.ToLower(retVal[i]);
			}

			return retVal.ToString();
		}

		private static string Cipher(string input, string key, bool encipher)
		{
			string retVal = string.Empty;
			char[,] keySquare = GenerateKeySquare(key);
			string tempInput = RemoveOtherChars(input);
			int e = encipher ? 1 : -1;

			if ((tempInput.Length % 2) != 0)
				tempInput += "X";

			for (int i = 0; i < tempInput.Length; i += 2)
			{
				int row1 = 0;
				int col1 = 0;
				int row2 = 0;
				int col2 = 0;

				GetPosition(ref keySquare, char.ToUpper(tempInput[i]), ref row1, ref col1);
				GetPosition(ref keySquare, char.ToUpper(tempInput[i + 1]), ref row2, ref col2);

				if (row1 == row2 && col1 == col2)
				{
					retVal += new string(SameRowColumn(ref keySquare, row1, col1, e));
				}
				else if (row1 == row2)
				{
					retVal += new string(SameRow(ref keySquare, row1, col1, col2, e));
				}
				else if (col1 == col2)
				{
					retVal += new string(SameColumn(ref keySquare, col1, row1, row2, e));
				}
				else
				{
					retVal += new string(DifferentRowColumn(ref keySquare, row1, col1, row2, col2));
				}
			}

			retVal = AdjustOutput(input, retVal);

			return retVal;
		}


		Code ICipherRepository.PlayFair(String body)
        {
            //drefix - add a different key option

			return new Code
			{
				body = Cipher(body,"cipher", true),
				style = "PlayFair"

			};
		}
    }
}
